package ch.ethz.globis.isk.persistence.jpa;

import ch.ethz.globis.isk.domain.*;
import ch.ethz.globis.isk.domain.jpa.JpaConference;
import ch.ethz.globis.isk.persistence.ConferenceDao;
import ch.ethz.globis.isk.persistence.ProceedingsDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Query;

@Repository
public class JpaConferenceDao extends JpaDao<String, Conference> implements ConferenceDao {

    @Autowired
    ProceedingsDao proceedingsDao;

    @Override
    protected Class<JpaConference> getStoredClass() {
        return JpaConference.class;
    }

    @Override
    public Conference createEntity() {
        return new JpaConference();
    }

    @Override
    public Conference findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Long countAuthorsForConference(String confId) {
//    	//query string
//    	String countAuthorsForConferenceString = "select count(author) from Person author, ConferenceEdition ce, Proceedings proceeding, Publication pub where ce.conference.id = :confID and proceeding.conferenceEdition = ce and pub in elements(proceeding.publications) and author in elements(pub.authors)";
//    	String countEditorsForConferenceString = "select count(editor) from Person editor, ConferenceEdition ce, Proceedings proceeding where ce.conference.id = :confID and proceeding.conferenceEdition = ce and editor in elements(proceeding.editors)";    	//query
//    	Query queryAuthors = em.createQuery(countAuthorsForConferenceString);
//    	Query queryEditors = em.createQuery(countEditorsForConferenceString);
//    	//set parameters for desired conference
//    	queryAuthors.setParameter("confID", confId);
//    	queryEditors.setParameter("confID", confId);
//    	//get results
//    	Long nOfAuthors = (long) queryAuthors.getResultList().get(0);
//    	Long nOfEditors = (long) queryEditors.getResultList().get(0);
//    	
//		System.out.println();
//    	System.out.println(nOfAuthors);
//		System.out.println();
//    	System.out.println(nOfEditors);
//		System.out.println();
//		
//    	return nOfAuthors + nOfEditors;
    	return new Long(findAuthorsForConference(confId).size());
    }

    @Override
    public Set<Person> findAuthorsForConference(String confId) {
    	Conference conference = findOne(confId);
    	Set<Person> authors = new HashSet<Person>();
    	Iterable<ConferenceEdition> confEditions = conference.getEditions();
    	for (ConferenceEdition conferenceEdition : confEditions) {
			Proceedings proceeding = conferenceEdition.getProceedings();
			authors.addAll(proceeding.getEditors());
			for (InProceedings inProceeding : proceeding.getPublications()) {
				authors.addAll(inProceeding.getAuthors());
			}
		}
    	return authors;
    }

    @Override
    public Set<Publication> findPublicationsForConference(String confId) {
    	//TODO check on correctness
    	String findPublicationsForConferencSring = "select pub from Publication pub, (SELECT ce.ID as conf_edition from Conference_edition ce where ce.conference_id = %s) where p.conference_edition_id = conf_edition";
    	findPublicationsForConferencSring = String.format(findPublicationsForConferencSring, confId);
    	Query findPublicationsForConferencQuery = em.createQuery(findPublicationsForConferencSring);
    	return new HashSet<Publication>(findPublicationsForConferencQuery.getResultList());
    }

    @Override
    public Long countPublicationsForConference(String confId) {
    	//TODO ok
    	String countProceedingsForConferenceString = "select count(p) as proceedingCount from Publication p, ConferenceEdition ce where ce.conference.id = :confID and p.conferenceEdition.id = ce.id";	                                                                                                                            
    	String countInProceedingsForConferenceString = "select count(inp) as inProceedingCount from Publication p, ConferenceEdition ce, InProceedings inp where ce.conference.id = :confID and p.conferenceEdition.id = ce.id and inp.proceedings.id = p.id";
 
    	Query countProceedingsForCoferenceQuery = em.createQuery(countProceedingsForConferenceString);
    	Query countInProceedingsForCoferenceQuery = em.createQuery(countInProceedingsForConferenceString);
    	
    	countProceedingsForCoferenceQuery.setParameter("confID", confId);
    	countInProceedingsForCoferenceQuery.setParameter("confID", confId);
    	
    	List<Object> proceedingsCountList = countProceedingsForCoferenceQuery.getResultList();
    	List<Object> inProceedingsCountList = countInProceedingsForCoferenceQuery.getResultList();
    	
    	long inProceedingCount = (long) inProceedingsCountList.get(0);
    	long proceedingCount = (long) proceedingsCountList.get(0);

    	return inProceedingCount + proceedingCount;
    }
}
