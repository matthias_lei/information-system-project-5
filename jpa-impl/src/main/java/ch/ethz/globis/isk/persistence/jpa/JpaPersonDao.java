package ch.ethz.globis.isk.persistence.jpa;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.jpa.JpaPerson;
import ch.ethz.globis.isk.persistence.PersonDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.utils.AuthorsDistanceCalculator;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

@Repository
public class JpaPersonDao extends JpaDao<String, Person> implements PersonDao {

    @Override
    protected Class<JpaPerson> getStoredClass() {
        return JpaPerson.class;
    }

    @Override
    public Person createEntity() {
        return new JpaPerson();
    }

    @Override
    public Person findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Set<Person> getCoauthors(String id) {
    	//TODO ok
//    	String findCoAuthorsQuery = "select coauthor from Publication pub, Person coauthor, Person author where coauthor in elements(pub.authors) and author in elements(pub.authors) and author.id = :id and coauthor.id <> :id";
//	    Query query = em.createQuery(findCoAuthorsQuery);
//	    query.setParameter("id", id);
//	    return new HashSet<Person>(query.getResultList());
    	Person author = findOne(id);
    	Set<Publication> publications = author.getAuthoredPublications();
    	Set<Person> coAuthors = new HashSet<Person>();
    	for (Publication publication : publications) {
			coAuthors.addAll(publication.getAuthors());
		}
    	coAuthors.remove(author);
    	return coAuthors;
    }

    @Override
    public Long computeAuthorDistance(String firstId, String secondId) {
    	AuthorsDistanceCalculator calculator = new AuthorsDistanceCalculator(this);
    	return calculator.getDistance(firstId, secondId);
    }

    private Set<Person> getCoauthors(Person person) {
    	//TODO: might be ok
//    	String findCoAuthorsQuery = "select coauthor from Publication pub, Person coauthor where coauthor in elements(pub.authors) and :author in elements(pub.authors) and coauthor.id <> :author";
//	    Query query = em.createQuery(findCoAuthorsQuery);
//	    query.setParameter("author", person);
//	    return new HashSet<Person>(query.getResultList());
	    
    	Set<Publication> publications = person.getAuthoredPublications();
    	Set<Person> coAuthors = new HashSet<Person>();
    	for (Publication publication : publications) {
			coAuthors.addAll(publication.getAuthors());
		}
    	coAuthors.remove(person);
    	return coAuthors;

   }
}
