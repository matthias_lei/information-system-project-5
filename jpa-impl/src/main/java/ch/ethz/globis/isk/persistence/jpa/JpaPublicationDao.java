package ch.ethz.globis.isk.persistence.jpa;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.jpa.JpaPublication;
import ch.ethz.globis.isk.persistence.PublicationDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.text.Format;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Query;

@Repository
public class JpaPublicationDao extends JpaDao<String, Publication> implements PublicationDao {

    private static String AVERAGE_NUMBER_OF_AUTHORS = "select avg(convert(count, double)) as average " + "from (select  publication_author.id, count(*) as count " + "from publication_author inner join person " + "on publication_author.person_id = person.id " + "group by publication_author.id)";

    private static String COUNT_PUBLICATIONS_PER_YEAR_INTERVAL = "select year, count(*) from publication " + "where year >= :startYear and year <= :endYear group by year";

    @Override
    protected Class<JpaPublication> getStoredClass() {
        return JpaPublication.class;
    }

    @Override
    public Publication createEntity() {
        return new JpaPublication();
    }

    @Override
    public Publication findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public Map<Long, Long> countPerYears(Long startYear, Long endYear) {
    	//TODO ok
    	//create query to get the number of publications in each year between startYear and end Year
    	String countPerYearsString = "select p.year,count(*) as number_of_publications from Publication p where p.year >= %d and p.year <= %d group by p.year order by p.year";
    	countPerYearsString = String.format(countPerYearsString,startYear,endYear);
    	Query countPerYearQuery = em.createQuery(countPerYearsString);
    	//get results
    	List<Object[]> resultList = countPerYearQuery.getResultList();
    	
    	TreeMap<Long, Long> resutlMap = new TreeMap<Long, Long>();
    	
    	for(Object[] o: resultList){
    		resutlMap.put(new Long(o[0].toString()),new Long(o[1].toString()));
    	}
    	return resutlMap;
    }

    @Override
    public Double getAverageNumberOfAuthors() {
    	//TODO error
		//create queries
//		Query nOfAuthrosQuery = em.createQuery("select count(pers) from Person pers, Publication pub where pers in elements(pub.authors)");
//		Double nOfAuthors = (Double) nOfAuthrosQuery.getSingleResult();
//		System.out.println(nOfAuthors);
//		
//		Query nOfPublicationsQuery = em.createQuery("select count(*) from (select  distinct pa.id from publication_author  pa)");
//		//get record count for number of authors and number of publications
//		Double nOfPublication = (Double) nOfPublicationsQuery.getSingleResult();
//		System.out.println(nOfPublication);
//		return nOfAuthors/nOfPublication;
    	
    	Iterable<Publication> publications = this.findAll();
    	double sum = 0;
    	double count = 0;
    	int size;
    	
    	for(Publication p : publications){
    		size = p.getAuthors().size();
    		if(size != 0) count++;
    		sum = sum + size;
    	}
    	Double res = sum/count;
    	return res;
    }

    @Override
    public List<Publication> findByAuthorIdOrderedByYear(String authorId) {
        return queryByReferenceIdOrderByYear("Publication", "authors", authorId);
    }

    @Override
    public List<Publication> findByEditorIdOrderedByYear(String editorId) {
        return queryByReferenceIdOrderByYear("Publication", "editors", editorId);
    }

    @Override
    public List<Publication> findByPublisherOrderedByYear(String publisherId) {
        return queryByReferenceIdOrderByYear("Publication", "publisher", publisherId);
    }

    @Override
    public List<Publication> findBySchoolOrderedByYear(String schoolId) {
        return queryByReferenceIdOrderByYear("Publication", "school", schoolId);
    }

    @Override
    public List<Publication> findBySeriesOrderedByYear(String seriesId) {
        return queryByReferenceIdOrderByYear("Publication", "series", seriesId);
    }
}
