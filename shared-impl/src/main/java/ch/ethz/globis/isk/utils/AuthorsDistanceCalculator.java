package ch.ethz.globis.isk.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import ch.ethz.globis.isk.domain.*;
import ch.ethz.globis.isk.persistence.PersonDao;

public class AuthorsDistanceCalculator {

	final HashMap<String, Integer> visited;
	final PersonDao personDao;
	final Queue<String> queue;
	
	public AuthorsDistanceCalculator(PersonDao personDao) {
		visited = new HashMap<String, Integer>();
		queue = new LinkedList<String>();
		this.personDao = personDao;
	}
	
	public Long getDistance(String startId, String endId){
		// Initialize
		Long result = (long) 0;
		queue.add(startId);
		visited.put(startId, 1);
		
		// Bread first search for the endPerson
		while (!queue.isEmpty()){
			//System.out.println("loop: " + queue.size());
			String currentId = queue.poll();
			
			if (currentId.equals(endId)){
				result = (long) visited.get(currentId);
				break;
			}
			
			
			for (Person nextPerson : personDao.getCoauthors(currentId)){
				String nextId = nextPerson.getId();
				if (!visited.keySet().contains(nextId)){
					queue.add(nextId);
					visited.put(nextId, visited.get(currentId) + 1);
				}
			}
			
		}
		
		// reset both Datastructure
		visited.clear();
		queue.clear();
		
		// return the distance
		return result;
	}
}
