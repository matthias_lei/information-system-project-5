package ch.ethz.globis.isk.persistence.db4o;

import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.db4o.Db4oPublication;
import ch.ethz.globis.isk.persistence.PublicationDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.db4o.ObjectSet;
import com.db4o.foundation.NotImplementedException;
import com.db4o.query.Query;

import java.util.*;

@Repository
public class Db4oPublicationDao extends Db4oDao<String, Publication> implements PublicationDao {

    @Override
    public Publication createEntity() {
        return new Db4oPublication();
    }

    @Override
    public Publication findOneByTitle(String title) {
        Map<String, Filter> filterMap = new TreeMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public Map<Long, Long> countPerYears(Long startYear, Long endYear) {
    	// create a new TreeMap for results
    	TreeMap<Long, Long> result = new TreeMap<Long,Long>();
    	
    	// loop over the years
    	for (int queryYear = (int) (long) startYear; queryYear <= endYear; queryYear++){
    		// create a new query
    		Query query = db.query();
    		
    		// constrain it
    		query.constrain(Db4oPublication.class);
    		query.descend("year").constrain(queryYear);
    		
    		// save the result
    		ObjectSet<Db4oPublication> set = query.execute();
    		Long key = new Long(queryYear);
    		Long value = new Long(set.size());
    		result.put(key, value);
    	}
    	
    	return result;
    }

    @Override
    public Double getAverageNumberOfAuthors() {
    	// create a new query
    	Query query = db.query();
    	
    	// constrain it
    	query.constrain(Db4oPublication.class);
    	
    	// save the result
    	ObjectSet<Db4oPublication> set = query.execute();
    	
    	// calculate the average
    	double authors = 0;
    	double publications = 0;
    	
    	for (Db4oPublication publication : set){
    		int pubauthors = publication.getAuthors().size();
    		
    		// if this is a publication with authors, then update the numbers
    		if (pubauthors != 0){
    			authors += pubauthors;
    			publications++;
    		}
    	}
    	double ret = authors/publications;
    	
    	// return the average
    	return ret;
    }

    @Override
    public Class getStoredClass() {
        return Db4oPublication.class;
    }

    @Override
    public List<Publication> findByAuthorIdOrderedByYear(String authorId) {
        return queryByReferenceIdOrderByYear("authors", authorId).execute();
    }

    @Override
    public List<Publication> findByEditorIdOrderedByYear(String editorId) {
        return queryByReferenceIdOrderByYear("editors", editorId).execute();
    }

    @Override
    public List<Publication> findByPublisherOrderedByYear(String publisherId) {
        return queryByReferenceIdOrderByYear("publisher", publisherId).execute();
    }

    @Override
    public List<Publication> findBySchoolOrderedByYear(String schoolId) {
        return queryByReferenceIdOrderByYear("school", schoolId).execute();
    }

    @Override
    public List<Publication> findBySeriesOrderedByYear(String seriesId) {
        return queryByReferenceIdOrderByYear("series", seriesId).execute();
    }
}
