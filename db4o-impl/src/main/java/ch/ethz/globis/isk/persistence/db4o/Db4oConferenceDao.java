package ch.ethz.globis.isk.persistence.db4o;

import ch.ethz.globis.isk.domain.*;
import ch.ethz.globis.isk.domain.db4o.Db4oConference;
import ch.ethz.globis.isk.domain.db4o.Db4oPerson;
import ch.ethz.globis.isk.domain.db4o.Db4oPublication;
import ch.ethz.globis.isk.persistence.ConferenceDao;
import ch.ethz.globis.isk.persistence.ProceedingsDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Constraint;
import com.db4o.query.Query;

import java.util.*;

@Repository
public class Db4oConferenceDao extends Db4oDao<String, Conference> implements ConferenceDao {

    @Autowired
    ProceedingsDao proceedingsDao;
    
    @Autowired
    protected ObjectContainer db;

    @Override
    public Conference findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Long countAuthorsForConference(String confId) {
//    	// get a new query
//    	Query query = db.query();
//    	
//    	// constrain it
//    	query.constrain(Db4oPerson.class);
//    	//query.descend("authoredPublications").descend("proceedings").descend("conferenceEdition").descend("conference").descend("id").constrain(confId);    	
//    	query.descend("editedPublications").descend("conferenceEdition").descend("conference").descend("id").constrain(confId);    	
//    	
//
//    	ObjectSet<Db4oPerson> result = query.execute();
//    	//HashSet<Db4oPerson> result = new HashSet<Db4oPerson>();
//
//    	return (long) result.size();
    	
    	Set<Person> set = findAuthorsForConference(confId);
    	return (long) set.size();
    }

    @Override
    public Set<Person> findAuthorsForConference(String confId) {
//    	// get a new query
//    	Query query = db.query();
//    	
//    	// constrain it
//    	query.constrain(Db4oPerson.class);
//    	query.descend("authoredPublications").descend("proceedings").descend("conferenceEdition").descend("conference").descend("id").constrain(confId);
//    	
//    	// execute and return the ObjectSet as TreeSet
//    	List<Person> result = query.execute();
//    	return new HashSet<Person>(result);
    	
    	HashSet<Person> set = new HashSet<Person>();
    	
    	Conference conference = findOne(confId);
    	
    	for ( ConferenceEdition confEd : conference.getEditions()){
    		Proceedings proceedings = confEd.getProceedings();
    		
    		set.addAll(proceedings.getEditors());
    		
    		for (InProceedings inProceedings : proceedings.getPublications()){
    			set.addAll(inProceedings.getAuthors());
    		}
    	}
    	
    	return set;
    }
    

    @Override
    public Set<Publication> findPublicationsForConference(String confId) {
//    	// get a new query
//    	Query query = db.query();
//    	
//    	// constrain it
//    	query.constrain(Db4oPublication.class);
//    	Constraint constr = query.descend("proceedings").descend("conferenceEdition").descend("conference").descend("id").constrain(confId);
//    	query.descend("conferenceEdition").descend("conference").descend("id").constrain(confId).or(constr);
//    	
//    	// execute and return the ObejctSet as Hashset
//    	ObjectSet<Publication> result = query.execute();
//    	return new HashSet<Publication>(result);
    	
    	HashSet<Publication> set= new HashSet<Publication>();
    	
    	Conference conference = findOne(confId);
    	    	
    	for ( ConferenceEdition confEd : conference.getEditions()){
    		Proceedings proceedings = confEd.getProceedings();
    		
    		set.add(proceedings);
    		set.addAll(proceedings.getPublications());
    	}
    	
    	
    	return set;
    	
    	
    }

    @Override
    public Long countPublicationsForConference(String confId) {
//    	// get a new query
//    	Query query = db.query();
//    	
//    	// constrain it
//    	query.constrain(Db4oPublication.class);
//    	Constraint constr = query.descend("proceedings").descend("conferenceEdition").descend("conference").descend("id").constrain(confId);
//    	query.descend("conferenceEdition").descend("conference").descend("id").constrain(confId).or(constr);
//    	
//    	// execute and return the ObejctSet as Hashset
//    	ObjectSet<Publication> result = query.execute();
//    	return (long) result.size();
    	
    	Set<Publication> set = findPublicationsForConference(confId);
    	return (long) set.size();
    }

    @Override
    public Conference createEntity() {
        return new Db4oConference();
    }

    @Override
    public Class getStoredClass() {
        return Db4oConference.class;
    }

    private Comparator personComparator() {
        return new Comparator<Person>() {

            @Override
            public int compare(Person o1, Person o2) {
                return o1.getId().compareTo(o2.getId());
            }
        };
    }

    private Comparator publicationComparator() {
        return new Comparator<Publication>() {

            @Override
            public int compare(Publication o1, Publication o2) {
                return o1.getId().compareTo(o2.getId());
            }
        };
    }
}
