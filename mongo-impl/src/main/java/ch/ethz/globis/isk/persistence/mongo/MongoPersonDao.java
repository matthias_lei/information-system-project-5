package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.persistence.PersonDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.utils.AuthorsDistanceCalculator;

import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class MongoPersonDao extends MongoDao<String, Person> implements PersonDao {

    @Override
    public Person findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Set<Person> getCoauthors(String id) {
    	Person author = findOne(id);
//    	String publications = "";
//    	Iterator<Publication> i = author.getAuthoredPublications().iterator();
//    	while(i.hasNext()){
//    		publications = publications + " \"" + i.next().getId() + "\"";
//    		if(i.hasNext()) publications = publications + ", ";
//    	}
//    	String query = "{ $and: [{\"authoredPublications.$id\": { $in :  [ " + publications + " ]}}, {_id : {$ne: \"" + id + "\"}}]}"; 	
//    	
//    	List<MongoPerson> coauthors = mongoOperations.find(new BasicQuery(query), getStoredClass(), collection());
//    	
//    	return new HashSet<Person>(coauthors);
    	Set<Publication> ps = author.getAuthoredPublications();
    	Set<Person> res = new HashSet<Person>();
    	for(Publication p : ps){
    		res.addAll(p.getAuthors());
    	}
    	res.remove(author);
    	return res;
    }

    @Override
    public Long computeAuthorDistance(String firstId, String secondId) {
    	AuthorsDistanceCalculator adc = new AuthorsDistanceCalculator(this);
    	return adc.getDistance(firstId, secondId);
    }

    @Override
    public Person createEntity() {
        return new MongoPerson();
    }

    @Override
    protected Class<MongoPerson> getStoredClass() {
        return MongoPerson.class;
    }

    @Override
    protected String collection() {
        return "person";
    }

    private Set<Person> getCoauthors(Person person) {
    	return null;
    }
}
