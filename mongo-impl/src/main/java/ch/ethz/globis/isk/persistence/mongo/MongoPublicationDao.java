package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.mongo.MongoPublication;
import ch.ethz.globis.isk.persistence.PublicationDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import com.mongodb.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

//remove annot
@Configuration
@Repository
public class MongoPublicationDao extends MongoDao<String, Publication> implements PublicationDao {
	
	//remove bean
	@Bean
	MongoPublicationDao mongoPublicationDao(){
		return new MongoPublicationDao();
	}
	
    @Override
    protected Query basicQuery() {
        return new Query();
    }

    @Override
    public Publication findOneByTitle(String title) {
        Map<String, Filter> filterMap = new TreeMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public Map<Long, Long> countPerYears(Long startYear, Long endYear) {
    	
    	
    	Map<Long, Long> result = new TreeMap<Long, Long>();
    	for(long i = startYear; i <= endYear; i ++){
    		Query query = new Query();
    		Criteria yearConstraint = Criteria.where("year").is(i);
    		query.addCriteria(yearConstraint);
    		//this.mongoOperations.find(query, getStoredClass(), collection());
    		result.put(i, mongoOperations.count(query, collection()));
    	}
    	//System.out.println("countPerYears: " + result.size());
    	return result;
    }

    @Override
    public Double getAverageNumberOfAuthors() {
    	
    	
//    	long start;
//    	long elapsed;
//    	
//    	
//		String map = "function() { " +
//				"emit(this._id, this.authors.length);}";
//		
//		String reduce = "function(key, values) { " +
//				"return values;}";
//		start = System.currentTimeMillis();
//		MapReduceResults<KeyValuePair> results = this.mongoOperations.mapReduce("publication", map, reduce, KeyValuePair.class);
//		//System.out.println("getAverageNumberOfAuthors sample: " + results.iterator().next().toString());
//		
//		Double sum = 0.0;
//		Double count = 0.0;
//	 
//		System.out.println("mapreduce: " + (System.currentTimeMillis() - start));
//		
//		start = System.currentTimeMillis();
//		for (KeyValuePair keyValuePair : results) {
//			if(Double.valueOf(keyValuePair.getValue()) != 0.0){
//				sum += Double.valueOf(keyValuePair.getValue());
//				count ++;
//			}
//		}
//		System.out.println("computing average: " + (System.currentTimeMillis() - start));
//		System.out.println("getAverageNumberOfAuthors result: " + (sum/count));
//		return count != 0.0 ? (sum/count) : 0.0;

    	Iterable<Publication> publications = this.findAll();
    	double sum = 0;
    	double count = 0;
    	int size;
    	
    	for(Publication p : publications){
    		size = p.getAuthors().size();
    		if(size != 0) count++;
    		sum = sum + size;
    	}
    	Double res = sum/count;
    	System.out.println("getAverageNumberOfAuthors: " + res);
    	return res;
    }

    @Override
    public Publication createEntity() {
        return new MongoPublication();
    }

    @Override
    protected Class<MongoPublication> getStoredClass() {
        return MongoPublication.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public List<Publication> findByAuthorIdOrderedByYear(String authorId) {
        return queryByReferenceIdOrderByYear("authors.$id", authorId);
    }

    @Override
    public List<Publication> findByEditorIdOrderedByYear(String editorId) {
        return queryByReferenceIdOrderByYear("editors.$id", editorId);
    }

    @Override
    public List<Publication> findByPublisherOrderedByYear(String publisherId) {
        return queryByReferenceIdOrderByYear("publisher.$id", publisherId);
    }

    @Override
    public List<Publication> findBySchoolOrderedByYear(String schoolId) {
        return queryByReferenceIdOrderByYear("school.$id", schoolId);
    }

    @Override
    public List<Publication> findBySeriesOrderedByYear(String seriesId) {
        return queryByReferenceIdOrderByYear("series.$id", seriesId);
    }
}

class KeyValuePair {

    @Id
    String key;

    String value;

    KeyValuePair() {
    }

    String getValue() {
        return value;
    }

    void setValue(String value) {
        this.value = value;
    }

    String getKey() {
        return key;
    }

    void setKey(String key) {
        this.key = key;
    }
    
    @Override
    public String toString(){
    	return "Key: " + key + ", " + value;
    }
}
